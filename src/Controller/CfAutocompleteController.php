<?php

namespace Drupal\cf_autocomplete\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides autocomplete handlers.
 */
class CfAutocompleteController extends ControllerBase {

  /**
   * Taxonomy term autocomplete handler.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   * @param $vid
   *   Vocabulary ID.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *  Json response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function handleTaxonomyTerm(Request $request, $vid): JsonResponse {

    $terms = $this->entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties([
        'vid' => $vid,
        'status' => 1,
      ]);
    $input = $request->query->get('q');
    $response = [];

    /** @var \Drupal\taxonomy\Entity\Term $term */
    foreach ($terms as $term) {
      if ((stripos($term->getName(), $input) !== FALSE)) {
        $response[] = [
          'value' => EntityAutocomplete::getEntityLabels([$term]),
          'label' => $term->getName(),
        ];
      }

    }

    return new JsonResponse($response);
  }

  /**
   * Node autocomplete handler.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request.
   * @param $content_type_id
   *   Node type id.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *  Json response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function handleNode(Request $request, $content_type_id): JsonResponse {

    $nodes = $this->entityTypeManager()
      ->getStorage('node')
      ->loadByProperties([
        'type' => $content_type_id,
        'status' => 1,
      ]);
    $input = $request->query->get('q');
    $response = [];

    /** @var \Drupal\node\NodeInterface $node */
    foreach ($nodes as $node) {
      if ((stripos($node->getTitle(), $input) !== FALSE)) {
        $response[] = [
          'value' => EntityAutocomplete::getEntityLabels([$node]),
          'label' => $node->getTitle(),
        ];
      }
    }

    return new JsonResponse($response);
  }

}
