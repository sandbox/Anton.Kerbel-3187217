#Custom Form Autocomplete

Provides handlers for autocomplete widgets in custom forms.

##Examples
- For taxonomy terms:
```
$form['taxonomy_term_id'] = [
    '#type' => 'textfield',
    '#title' => $this->t('Choose category')',
    '#autocomplete_route_name' => 'cf_autocomplete.taxonomy_term',
    '#autocomplete_route_parameters' => [
        'vid' => 'categories',
    ],
];
```
- For nodes:
```
$form['node_id'] = [
    '#type' => 'textfield',
    '#title' => $this->t('Choose node')',
    '#autocomplete_route_name' => 'cf_autocomplete.node',
    '#autocomplete_route_parameters' => [
        'content_type_id' => 'page',
    ],
];
```


##Gettings data from autocomplete fields:

```
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Form\FormStateInterface;

public function submitForm(array &$form, FormStateInterface $form_state) {
    ...
    $node_id = EntityAutocomplete::extractEntityIdFromAutocompleteInput($form_state->getValue('node_id'))
    ...
}
```
